package co.com.reto3.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.reto3.screenplay.questions.ElMensaje;
import co.com.reto3.screenplay.tasks.Abrir;
import co.com.reto3.screenplay.tasks.Agendar;
import co.com.reto3.screenplay.tasks.Registrar;
import co.com.reto3.screenplay.tasks.RegistrarPaciente;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class SistemaAdministracionHospitalStepDefinitions {

	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor Juan = Actor.named("Juan Carlos");
	
	@Before
	public void configuracionInicial() {
		Juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que Carlos necesita registrar un nuevo doctor$")
	public void queCarlosNecesitaRegistrarUnNuevoDoctor() throws Exception {
		Juan.wasAbleTo(Abrir.LaPaginaSistemaDeAdministracionDeHospitales());
	}
	

	@Cuando("^el realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelMismoEnElAplicativoDeAdministraciNDeHospitales(DataTable TableDoctor) throws Exception {
		List<List<String>> DataDoctor =  TableDoctor.raw();
		Juan.attemptsTo(Registrar.DatosDeElDoctorEnELFormulario(DataDoctor));
	}
	
	@Dado("^que Carlos necesita registrar un nuevo paciente$")
	public void queCarlosNecesitaRegistrarUnNuevoPaciente() throws Exception {
		Juan.wasAbleTo(Abrir.LaPaginaSistemaDeAdministracionDeHospitales());
	}
	
	@Cuando("^el realiza el registro del paciente en el mismo en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelPacienteEnElMismoEnElAplicativoDeAdministraciónDeHospitales(DataTable TablePaciente) throws Exception {
		List<List<String>> dataPaciente = TablePaciente.raw();
		Juan.attemptsTo(RegistrarPaciente.EnElFormularioDeAgregarPaciente(dataPaciente));
	}



	@Entonces("^el verifica que se presente en pantalla el mensaje (.*)$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamente(String mensaje) throws Exception {
		Juan.should(GivenWhenThen.seeThat(ElMensaje.es(), Matchers.equalTo(mensaje)));
	}
	
	@Dado("^que Carlos necesita asistir al medico$")
	public void queCarlosNecesitaAsistirAlMedico() throws Exception {
		Juan.wasAbleTo(Abrir.LaPaginaSistemaDeAdministracionDeHospitales());
	}


	@Cuando("^el realiza el agendamiento de una Cita$")
	public void elRealizaElAgendamientoDeUnaCita(DataTable TableCita) throws Exception {
	   List<List<String>> DataCita= TableCita.raw();
	   Juan.wasAbleTo(Agendar.LaCitaMedicaEnElFormulario(DataCita));
	}
}
