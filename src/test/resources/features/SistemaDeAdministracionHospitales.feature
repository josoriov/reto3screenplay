#Author: jucosori@bancolombia.com.co
#language:es
	
@tag
Característica: Web Automation Demo Site
Como usuario
Quiero ingresar al Web Automation Demo Site
para verificar que se carga la pantalla contexto  

  @tag1
	Escenario: Realizar el Registro de un Doctor
	Dado que Carlos necesita registrar un nuevo doctor
	Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales
	|<Nombre>			|<Apellidos>		|<Telefono>	|<TipoDocumento>			|<Documento>		|
	|Juan Carlos	|	Osorio Vásquez|6009587		|Cédula de ciudadanía	|1313131212			|
	Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
	
	@tag2
	Escenario: Realizar el Registro de un Paciente
	Dado que Carlos necesita registrar un nuevo paciente
	Cuando el realiza el registro del paciente en el mismo en el aplicativo de Administración de Hospitales
	|<Nombre Paciente>			|<Apellidos paciente>			|<Telefono>	|<TipoDocumento>			|<Documento>		|<Prepagada >	|
	|Juan jose							|	Fernandez Molina				|60092347		|Cédula de ciudadanía	|12313131				|Si						|
	Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
	
	@tag3
	Escenario: Realizar el Agendamiento de una Cita
	Dado que Carlos necesita asistir al medico
	Cuando el realiza el agendamiento de una Cita
	|<Fecha>		|<DocumentoPaciente>			|<DocumentoDoctor>	|<Observaviones>			|
	|10/11/2018	|12313131									|1313131212					|Cita Medica					|
	Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.
	
	
