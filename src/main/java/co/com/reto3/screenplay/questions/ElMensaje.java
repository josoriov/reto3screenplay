package co.com.reto3.screenplay.questions;

import co.com.reto3.screenplay.ui.WebAdministracionHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ElMensaje implements Question<String>{

	
	@Override
	public String answeredBy(Actor actor) {
		return Text.of(WebAdministracionHospitalesPage.LBL_MENSAJE).viewedBy(actor).asString();
	}

	public static ElMensaje es() {
		return new ElMensaje();
	}
	

}
