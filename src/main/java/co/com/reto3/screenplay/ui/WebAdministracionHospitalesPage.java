package co.com.reto3.screenplay.ui;

import javax.xml.xpath.XPath;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class WebAdministracionHospitalesPage extends PageObject {

	//Agregar doctor
	
	public static final Target BOTON_AGREGAR_DOCTOR = Target.the("BOT�N AGREGAR DOCTOR").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[1]"));
	public static final Target CAMPO_NOMBRE = Target.the("CAMPO NOMBRE COMPLETO").located(By.id("name"));
	public static final Target CAMPO_APPELLIDOS = Target.the("CAMPO APELLIDOS").located(By.id("last_name"));
	public static final Target CAMPO_TELEFONO = Target.the("CAMPO TELEFONO").located(By.id("telephone"));
	public static final Target SELECT_TIPO_IDENTIDAD = Target.the("TIPO DE DOCUMENTO DE IDENTIDAD").located(By.id("identification_type"));
	public static final Target CAMPO_NUMERO_IDENTIDAD = Target.the("CAMPO N�MERO DE DOCUMENTO DE IDENTIDAD").located(By.id("identification"));
	public static final Target BOTON_GUARDAR = Target.the("BOT�N GUARDAR DOCTOR").locatedBy("//*[@id=\'page-wrapper\']/div/div[3]/div/a");
	public static final Target LBL_MENSAJE = Target.the("MENSAJE DE GUARDADO").locatedBy("//*[@id=\"page-wrapper\"]/div/div[2]/div[2]/p");

	//Agregar paciente
	public static final Target BOTON_AGREGAR_PACIENTE = Target.the("BOT�N AGREGAR PACIENTE").located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div/div/div/div/div[1]/div/a[2]"));
	public static final Target CAMPO_NOMBRE_PACIENTE = Target.the("CAMPO NOMBRE COMPLETO DEL PACIENTE").located(By.name("name"));
	public static final Target CAMPO_APPELLIDOS_PACIENTE = Target.the("CAMPO APELLIDOS PACIENTE").located(By.name("last_name"));
	public static final Target CAMPO_TELEFONO_PACIENTE  = Target.the("CAMPO TELEFONO PACIENTE ").located(By.name	("telephone"));
	public static final Target SELECT_TIPO_IDENTIDAD_PACIENTE = Target.the("TIPO DE DOCUMENTO DE IDENTIDAD PACIENTE").located(By.name("identification_type"));
	public static final Target CAMPO_NUMERO_IDENTIDAD_PACIENTE = Target.the("CAMPO N�MERO DE DOCUMENTO DE IDENTIDAD PACIENTE").located(By.name("identification"));
	public static final Target CHECKBOX_SALUD_PREPAGADA = Target.the("TIPO DE SALUD DEL PACIENTE").located(By.name("prepaid"));
	
	//Agendar Cita
	public static final Target BOTON_AGENDAR_CITA = Target.the("BOT�N AGENDAR CITA").locatedBy("// A[@href='appointmentScheduling']");
	public static final Target CAMPO_FECHA_CITA = Target.the("CAMPO FECHA DE CITA").located(By.id("datepicker"));
	public static final Target CAMPO_DOCUMENTO_PACIENTE = Target.the("CAMPO DOCUMENTO PACIENTE").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/div[2]/input"));
	public static final Target CAMPO_DOCUEMNTO_DOCTOR = Target.the("CAMPO DOCUMENTO DOCTOR").located(By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/div[3]/input"));
	public static final Target CAMPO_OBSERVACIONES = Target.the("CAMPO OBSERVACIONES DE LA CITA").located(org.openqa.selenium.By.xpath("//*[@id=\'page-wrapper\']/div/div[3]/div/div[4]/textarea"));

	
}