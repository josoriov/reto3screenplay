package co.com.reto3.screenplay.tasks;

import co.com.reto3.screenplay.ui.WebAdministracionHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class Abrir implements Task {

	private WebAdministracionHospitalesPage webAdministracionHospitalesPage;
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(webAdministracionHospitalesPage));
	}


	public static Abrir LaPaginaSistemaDeAdministracionDeHospitales() {
		return Tasks.instrumented(Abrir.class);
	}
	
	

}
