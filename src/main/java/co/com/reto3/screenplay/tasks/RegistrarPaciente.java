package co.com.reto3.screenplay.tasks;

import java.util.List;

import co.com.reto3.screenplay.ui.WebAdministracionHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.targets.Target;

public class RegistrarPaciente implements Task {

	private List<List<String>> dataPaciente;
	
	
	public RegistrarPaciente(List<List<String>> dataPaciente) {
		super();
		this.dataPaciente = dataPaciente;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
			Click.on(WebAdministracionHospitalesPage.BOTON_AGREGAR_PACIENTE),
			Enter.theValue(dataPaciente.get(1).get(0).trim()).into(WebAdministracionHospitalesPage.CAMPO_NOMBRE_PACIENTE),
			Enter.theValue(dataPaciente.get(1).get(1).trim()).into(WebAdministracionHospitalesPage.CAMPO_APPELLIDOS_PACIENTE),
			Enter.theValue(dataPaciente.get(1).get(2).trim()).into(WebAdministracionHospitalesPage.CAMPO_TELEFONO_PACIENTE),
			SelectFromOptions.byVisibleText(dataPaciente.get(1).get(3).trim()).from(WebAdministracionHospitalesPage.SELECT_TIPO_IDENTIDAD_PACIENTE),
			Enter.theValue(dataPaciente.get(1).get(4).trim()).into(WebAdministracionHospitalesPage.CAMPO_NUMERO_IDENTIDAD_PACIENTE)
			);
		if(dataPaciente.get(1).get(5).trim().equals("Si")) {
			actor.attemptsTo(Click.on(WebAdministracionHospitalesPage.CHECKBOX_SALUD_PREPAGADA));	
		}
		actor.attemptsTo(Click.on(WebAdministracionHospitalesPage.BOTON_GUARDAR));
			
		
		
	}

	public static RegistrarPaciente EnElFormularioDeAgregarPaciente(List<List<String>> dataPaciente) {
		return Tasks.instrumented(RegistrarPaciente.class, dataPaciente);
	}

}
