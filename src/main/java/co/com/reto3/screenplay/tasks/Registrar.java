package co.com.reto3.screenplay.tasks;

import java.util.List;


import co.com.reto3.screenplay.ui.WebAdministracionHospitalesPage;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class Registrar implements Task {

	private List<List<String>> dataDoctor;
	
	public Registrar(List<List<String>> dataDoctor) {
		super();
		this.dataDoctor = dataDoctor;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
	
		actor.attemptsTo(		
				Click.on(WebAdministracionHospitalesPage.BOTON_AGREGAR_DOCTOR),
				Enter.theValue(dataDoctor.get(1).get(0).trim()).into(WebAdministracionHospitalesPage.CAMPO_NOMBRE),
				Enter.theValue(dataDoctor.get(1).get(1).trim()).into(WebAdministracionHospitalesPage.CAMPO_APPELLIDOS),
				Enter.theValue(dataDoctor.get(1).get(2).trim()).into(WebAdministracionHospitalesPage.CAMPO_TELEFONO),
				SelectFromOptions.byVisibleText(dataDoctor.get(1).get(3).trim()).from(WebAdministracionHospitalesPage.SELECT_TIPO_IDENTIDAD),
				Enter.theValue(dataDoctor.get(1).get(4).trim()).into(WebAdministracionHospitalesPage.CAMPO_NUMERO_IDENTIDAD),
				Click.on(WebAdministracionHospitalesPage.BOTON_GUARDAR)
		);
		
	}

	public static Registrar DatosDeElDoctorEnELFormulario(List<List<String>> dataDoctor) {
		return Tasks.instrumented(Registrar.class, dataDoctor);
	}

}
