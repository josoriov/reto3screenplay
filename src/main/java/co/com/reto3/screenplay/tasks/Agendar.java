package co.com.reto3.screenplay.tasks;

import java.util.List;

import co.com.reto3.screenplay.ui.WebAdministracionHospitalesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Agendar implements Task {

	private List<List<String>> dataCita;
	
	public Agendar(List<List<String>> dataCita) {
		super();
		this.dataCita = dataCita;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
	   actor.attemptsTo(
		Click.on(WebAdministracionHospitalesPage.BOTON_AGENDAR_CITA),
		Enter.theValue(dataCita.get(1).get(0).trim()).into(WebAdministracionHospitalesPage.CAMPO_FECHA_CITA),
		Enter.theValue(dataCita.get(1).get(1).trim()).into(WebAdministracionHospitalesPage.CAMPO_DOCUMENTO_PACIENTE),
		Enter.theValue(dataCita.get(1).get(2).trim()).into(WebAdministracionHospitalesPage.CAMPO_DOCUEMNTO_DOCTOR),
		Enter.theValue(dataCita.get(1).get(3).trim()).into(WebAdministracionHospitalesPage.CAMPO_OBSERVACIONES),
		Click.on(WebAdministracionHospitalesPage.BOTON_GUARDAR)
		);
	}

	public static Agendar LaCitaMedicaEnElFormulario(List<List<String>> dataCita) {

		return Tasks.instrumented(Agendar.class, dataCita);
	}

}
